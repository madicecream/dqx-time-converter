/**
 * Created by Bruce Zeng on 2017/3/3.
 */
var DQXTimeConverter;
(function (DQXTimeConverter_1) {
    var DQXTimeConverter = (function () {
        function DQXTimeConverter() {
            var _this = this;
            this.update = function () {
                var now = new Date();
                _this.earthSpan.innerText = _this.timeToString(now.getHours()) + ":" + _this.timeToString(now.getMinutes()) + ":" + _this.timeToString(now.getSeconds());
                var totalSeconds = now.getHours() * 60 * 60 + now.getMinutes() * 60 + now.getSeconds();
                //let totalSeconds = 60;
                totalSeconds -= 600;
                while (totalSeconds < 0) {
                    totalSeconds += 24 * 60 * 60;
                }
                var convertedTotalSeconds = totalSeconds * 20;
                var convertedTotalMinutes = Math.floor(convertedTotalSeconds / 60);
                convertedTotalSeconds = convertedTotalSeconds - convertedTotalMinutes * 60;
                var convertedTotalHours = Math.floor(convertedTotalMinutes / 60);
                convertedTotalMinutes = convertedTotalMinutes - convertedTotalHours * 60;
                convertedTotalHours = convertedTotalHours % 24;
                _this.dqxSpan.innerHTML = _this.timeToString(convertedTotalHours) + ":" + _this.timeToString(convertedTotalMinutes) + ":" + _this.timeToString(convertedTotalSeconds);
            };
            this.earthSpan = document.getElementById("earth-clock");
            this.dqxSpan = document.getElementById("dqx-clock");
            //this.dqxSpan.innerText = new Date().toUTCString();
            this.update();
        }
        DQXTimeConverter.prototype.start = function () {
            this.timerToken = setInterval(this.update, 500);
        };
        DQXTimeConverter.prototype.timeToString = function (time) {
            if (time < 10) {
                return "0" + time.toString();
            }
            else {
                return time.toString();
            }
        };
        DQXTimeConverter.prototype.stop = function () {
            clearTimeout(this.timerToken);
        };
        return DQXTimeConverter;
    }());
    window.onload = function () {
        var converter = new DQXTimeConverter();
        converter.start();
    };
})(DQXTimeConverter || (DQXTimeConverter = {}));
